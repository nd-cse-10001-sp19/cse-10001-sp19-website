title:      "Lab 03: Fizz Buzz Boom, Palindromes"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    The goal of this lab assignment to allow you to explore **repeated
    execution** by writing [Python] programs that utilize `for` and `while`
    loops to solve common programming interview challanges.

    For this assignment, record your work in a [Jupyter Notebook] and upload it
    to the [form] below by **11:59 PM Friday, February 15**.

    <div class="alert alert-warning" markdown="1">
    #### <i class="fa fa-warning"></i> Internet Resources

    While you are generally encouraged to use the Internet as a resource for
    programming assignments, for this lab, you are asked to **refrain** from
    looking up information specific to the these problems because the
    programming challanges are so popular and there are many solutions
    available.

    Remember that any **code** you submit must be **your own** and that you
    must be able to explain every line of your program.

    </div>

    ## Activity 0: Starter Notebook

    To help you get started, we have provided you with a [starter notebook],
    which you can download and then use to record your answers.  To utilize the
    [starter notebook], download it to wherever you are running [Jupyter
    Notebook] and then use the [Jupyter Notebook] interface to open the
    downloaded file.

    <div class="text-center">
      <p><a href="static/ipynb/lab03.ipynb" class="btn btn-success" download>Download Starter Notebook</a></p>
    </div>

    The [starter notebook] is already formatted for all the various sections
    you need to fill out.  Just look for the <font color="red">**red**</font>
    instructions that indicates were you need to write text or the <font
    color="cyan">**cyan**</font> comments that indicate where you need to write
    code.

    ## Activity 1: Fizz Buzz Boom

    For the first activity, you are to write a solution to a variant of the
    infamous [FizzBuzz] problem:

    > Write a program that prints the numbers from `1` to `100`. But for
    multiples of three print "Fizz" instead of the number and for the multiples
    of five print "Buzz". For numbers which are multiples of both three and
    five print "FizzBuzz".

    For this activity, the problem has a few additional requirements:

    - For multiples of seven, print "Boom".
    - For multiples of three and seven, print "FizzBoom".
    - For multiples of five and seven, print "BuzzBoom".
    - For multiples of three, five, and seven, print "FizzBuzzBoom".

    [FizzBuzz]: http://imranontech.com/2007/01/24/using-fizzbuzz-to-find-developers-who-grok-coding/

    ### Function

    To write this program, you will need to implement the following function:

        :::python
        def fizz_buzz_boom(start, end):
            ''' For all the numbers between start and end (inclusive),

            - Display Fizz for multiples of 3
            - Display Buzz for multiples of 5
            - Display Boom for multiples of 7
            - Display FizzBuzz for multiples of both 3 and 5
            - Display FizzBoom for multiples of both 3 and 7
            - Display BuzzBoom for multiples of both 5 and 7
            - Display FizzBuzzBoom for multiples of 3, 5, and 7
            - Display the number if it doesn't meet any of the criteria above.
            '''

    Here are some examples of the function in action:

        :::python
        >>> fizz_buzz_boom(3, 7)
        Fizz
        4
        Buzz
        Fizz
        Boom

        >>> fizz_buzz_boom(10, 20)
        Buzz
        11
        Fizz
        13
        Boom
        FizzBuzz
        16
        17
        Fizz
        19
        Buzz

    The `fizz_buzz_boom` function takes in the `start` and `end` of the range
    to iterate through:

    - It should include both the `start` and the `end`.

    - It should output the appropriate number or word based on the requirements
      specified above for [FizzBuzz]**Boom**.

    - It should use both **conditional** and **repeated execution** statements.

    - It should use the [modulo] operator.

    [modulo]: https://docs.python.org/3/reference/expressions.html#binary-arithmetic-operations

    ### Testing

    Once you have implemented the function above, you are to use the following
    `A1_TESTS` list to verify your code is working properly:

        :::python
        A1_TESTS = [
           [3, 7],
           [1, 10],
           [10, 35],
           [100, 110],
        ]

    You are to write a **loop** that uses each sub-list in `A1_TESTS` as the
    `start` and `end` parameters to the `fizz_buzz_boom` function and outputs
    the following:

        :::text
        fizz_buzz_boom(3, 7)
        Fizz
        4
        Buzz
        Fizz
        Boom

        fizz_buzz_boom(1, 10)
        1
        2
        Fizz
        4
        Buzz
        Fizz
        Boom
        8
        Fizz
        Buzz

        fizz_buzz_boom(10, 35)
        Buzz
        11
        Fizz
        13
        Boom
        FizzBuzz
        16
        17
        Fizz
        19
        Buzz
        FizzBoom
        22
        23
        Fizz
        Buzz
        26
        Fizz
        Boom
        29
        FizzBuzz
        31
        32
        Fizz
        34
        BuzzBoom

        fizz_buzz_boom(100, 110)
        Buzz
        101
        Fizz
        103
        104
        FizzBuzzBoom
        106
        107
        Fizz
        109
        Buzz

    ### Reflection

    After you have completed the program above, answer the following questions:

    1. Describe the **flow control** of your program. What sort of
    **conditional** or **repeated execution** statements did you use?

    2. Why do you think this question often trips up candidates interviewing
    for programming positions? What was the **trickiest** part of this problem
    for you?

    ## Activity 2: Palindromes

    For the second activity, you are to write a solution to a variant of the
    popular [palindromes] problem:

    > A palindrome is a word, number, phrase, or other sequence of characters
    which reads the same backward as forward, such as "madam" or "racecar" or
    the number `10801`.

    In this variant, you will be given a list of `numbers` and you must
    determine if the sequence can be read the same backward as forward.

    [palindromes]: https://en.wikipedia.org/wiki/Palindrome

    ### Function

    To write this program, you will need to implement the following function:

        :::python
        def is_palindrome(numbers):
          ''' Return True if the numbers list is a palindromic sequence,
          otherwise return False '''

    Here are some examples of the function in action:

        :::python
        >>> is_palindrome([1, 2])
        False

        >>> is_palindrome([1, 2, 1])
        True

    The `is_palindrome` function takes in a list of `numbers` and returns
    `True` if it is a palindrome and `False` otherwise:

    - It should keep track of both the front and back of the list.

    - It should use both **conditional** and **repeated execution** statements.

    ### Tests

    Once you have implemented the function above, you are to use the following
    `A2_TESTS` list to verify your code is working properly:

        :::python
        A2_TESTS = [
            [1],
            [1, 2],
            [1, 2, 1],
            [1, 2, 2, 1],
            [1, 2, 3, 1],
            [1, 3, 2, 1],
            [1, 2, 3, 2, 1],
        ]

    You are to write a **loop** that uses each sub-list in `A2_TESTS` as the
    `numbers` parameter to the `is_palindrome` function and outputs the
    following:

        :::text
        is_palindrome([1]) -> True
        is_palindrome([1, 2]) -> False
        is_palindrome([1, 2, 1]) -> True
        is_palindrome([1, 2, 2, 1]) -> True
        is_palindrome([1, 2, 3, 1]) -> False
        is_palindrome([1, 3, 2, 1]) -> False
        is_palindrome([1, 2, 3, 2, 1]) -> True

    ### Reflection
    
    After you have completed the program above, answer the following questions:

    1. Describe the **flow control** of your program. What sort of
    **conditional** or **repeated execution** statements did you use?

    2. Why do you think this question often trips up candidates interviewing
    for programming positions? What was the **trickiest** part of this problem
    for you?

    ## Submission

    Once you have completed your lab, submit your [Jupyter Notebook] using the [form]:

    <div class="text-center">
        <a href="https://goo.gl/forms/hOWU8AE15MpNo6C92" class="btn btn-danger">Submit Lab Notebook</a>
    </div>

    [Python]: https://www.python.org/
    [Jupyter Notebook]: https://jupyter.org/
    [starter notebook]: https://nbviewer.jupyter.org/urls/www3.nd.edu/~pbui/teaching/cse.10001.sp19/static/ipynb/lab03.ipynb
    [Markdown]: https://daringfireball.net/projects/markdown/
    [HTML]: https://developer.mozilla.org/en-US/docs/Web/HTML
    [form]: https://goo.gl/forms/hOWU8AE15MpNo6C92
