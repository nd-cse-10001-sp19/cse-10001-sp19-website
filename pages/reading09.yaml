title:      "Reading 09: Web Programming (HTML)"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Next week, we will begin discussing **web programming** and introduce
    creating web pages with [HTML].

    <div class="alert alert-info" markdown="1">
    #### <i class="fa fa-bookmark"></i> TL;DR

    For this week, you need to read about the [Internet], the [World Wide Web],
    and [HTML].  Afterwards, you will need to complete the [quiz] below.

    </div>

    <img src="static/img/digital.png" class="img-responsive pull-right" style="height: 175px">

    ## Readings

    The readings for this week are:

    1. [Understanding the Digital World]:

        - <p>9. The Internet (*Focus on sections 9.1 - 9.4*)</p>

        - <p>10. The World Wide Web (*Focus on sections 10.1 - 10.3*)</p>

    2. [Mozilla Developer Network]:

        - [How does the Internet work?](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/How_does_the_Internet_work)

        - [How the Web works](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/How_the_Web_works)

        - <p>[HTML basics](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics)</p>

    3. [Code Academy]:

        - [Introduction to HTML] (**Optional tutorial**)

    [Introduction to HTML]: https://www.codecademy.com/learn/learn-html
    [Code Academy]:         https://www.codecademy.com

    <div class="alert alert-success" markdown="1">
    #### <i class="fa fa-code"></i> The Hands-on Imperative

    To get the most out of your reading, you should be **typing** commands into
    the computer and **playing** around with the things you reading.  Passively
    reading will not be as fruitful as **actively** reading and trying out
    things you are exploring.

    </div>

    ## Quiz

    Once you have completed the readings, fill out the following [quiz]:

    <div class="text-center">
    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScJ2jrTZBB8qcvGUzXco0OKxkDRPff38hy9FjWs5btENi0M_w/viewform?embedded=true" width="800" height="600" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
    </div>

    If you cannot see the embedded [quiz] above, you can click on the button
    below to go to the [quiz] form:

    <div class="text-center">
    <p><a class="btn btn-primary" href="https://forms.gle/TTgih8YVMcWF6Jck6">Reading Quiz</a></p>
    </div>

    <div class="alert alert-danger" markdown="1">
    #### <i class="fa fa-warning"></i> Notre Dame Login

    To view and submit the form below, you need to be logged into your Notre Dame
    Google account.  The easiest way to do this is to login to
    [gmail.nd.edu](https://gmail.nd.edu) and then visit this page in the same
    browser session.

    </div>

    **Note**, you can view the initial quiz score after you submit your
    responses.  If you get any answers wrong, you can go back and adjust your
    answers as necessary.  After the deadline has passed, any wrong answers
    will be given partial credit.

    [Python]:                                   https://www.python.org/
    [Understanding the Digital World]:          http://kernighan.com/udw.html
    [Automate the Boring Stuff with Python]:    https://automatetheboringstuff.com/
    [quiz]:                                     https://forms.gle/TTgih8YVMcWF6Jck6

    [Mozilla Developer Network]: https://developer.mozilla.org/en-US/
    [HTML]: https://www.w3.org/html/
