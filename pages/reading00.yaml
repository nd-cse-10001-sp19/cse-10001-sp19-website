title:      "Reading 00: Hardware, Programming"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Welcome to [CSE 10001 Principles of Computing](index.html), which (as the
    syllabus states) is a course that "explores computer science by examining
    the questions of what is computing, how do we compute, how do different
    aspects of modern computing technology work, and how does computing impact
    the individual and society."  In this course, we will study how modern
    computing hardware, software, and communications function, and we will
    utilize the [Python] programming language to explore and practice
    foundational programming concepts such as syntax, variables, conditional
    execution, iteration, functions, data structures, and algorithms.

    <div class="alert alert-info" markdown="1">
    #### <i class="fa fa-bookmark"></i> TL;DR

    For this week, you need to read about what's in a computer and then a
    general overview of programming in [Python].  Afterwards, you will need the
    [quiz] below.
    </div>

    ## Course Overview

    Today, computing is everywhere and touches almost every part of modern
    life.  From automation to disruptive industries, social media to fake news,
    ubiquitous communication to surveillance, personal assistants to data
    mining, and so on, it is now paramount that the everyone has a basic
    understanding of concepts and principles that power our digital world.

    In this class, we will explore the foundational ideas behind computer
    science by examining these three fundamental components of modern computing:

    1. **Hardware**:  We will examine the idea of a *universal digital
    representation of information* and learn how data is represented using
    [binary codes] and how physical devices use this representation to compute.

    2. **Software**:  We will utilize the idea of a *universal digital
    processor* by exploring different programming concepts such as [data
    structures] and [algorithms].

    3. **Communications**: We will study the idea of a *universal digital
    network*, learn how the Internet works, and discuss the ramifications of
    widespread digital communication.

    In addition to examining these fundamental concepts and principles, we will
    also develop practical programming skills by utilizing the [Python]
    programming language to develop applications as an emoji translator, spell
    checker, image filter, online guest book, [Twitter] bot, and password
    cracker.  Before tackling these programs, however, we will first learn the
    basics of programming by studying syntax, expressions, conditional
    execution, iteration, functions, [data structures], and [algorithms].

    Each week there will be a **reading assignment** meant to give you a
    context and reference into what we will be covering that upcoming week and
    a **lab assignment** that will provide you an opportunity apply the
    material in (hopefully) interesting ways.

    To help with completing the labs, the class will meet in the computer room
    in **B019 Fitzpatrick Hall** each **Friday**.  This is so students can
    begin work on the week's lab with the assistance of the instructors and
    teaching assistants and receive more immediate and direct programming
    guidance and instruction.

    Finally, there will be **two exams** to assess your mastery of the
    material.

    <img src="static/img/automate.png" class="img-responsive pull-right" style="height: 175px">
    <img src="static/img/digital.png" class="img-responsive pull-right" style="height: 175px">

    ## Readings

    The readings for **Wednesday, January 16** are:

    1. [Understanding the Digital World]:

        - Introduction
        - Hardware
        - <p>1. What's in a Computer?</p>

    2. [Automate the Boring Stuff With Python]:

        - [Chapter 0 - Introduction](https://automatetheboringstuff.com/chapter0/)

    <div class="alert alert-success" markdown="1">
    #### <i class="fa fa-terminal"></i> The Hands-on Imperative

    To get the most out of your reading, you should be **typing** commands into
    a [Python] interpreter and **playing** around with the things you reading.

    Passively reading will not be as fruitful as **actively** reading and
    trying out things you are exploring.
    </div>

    ## Quiz

    Once you have completed the readings, fill out the following [quiz]:

    <div class="text-center">
    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSd388kXsHmH-TVHK5H1YD4C1Ly6tDvCZwzQHddslSJ5WDZYeA/viewform?embedded=true" width="800" height="600" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
    </div>

    <div class="alert alert-danger" markdown="1">
    #### <i class="fa fa-warning"></i> Notre Dame Login

    To view and submit the form below, you need to be logged into your Notre Dame
    Google account.  The easiest way to do this is to login to
    [gmail.nd.edu](https://gmail.nd.edu) and then visit this page in the same
    browser session.

    </div>

    **Note**, you can view the initial quiz score after you submit your
    responses.  If you get any answers wrong, you can go back and adjust your
    answers as necessary.  After the deadline has passed, any wrong answers
    will be given partial credit.

    [Python]:                                   https://www.python.org/
    [Understanding the Digital World]:          http://kernighan.com/udw.html
    [Automate the Boring Stuff with Python]:    https://automatetheboringstuff.com/
    [binary codes]:                             https://en.wikipedia.org/wiki/Binary_code
    [data structures]:                          https://en.wikipedia.org/wiki/Data_structure
    [algorithms]:                               https://en.wikipedia.org/wiki/Algorithm
    [twitter]:                                  https://twitter.com/
    [quiz]:                                     https://goo.gl/forms/0FUCS5uORTvX01kJ2
