title:      Checklist 02
icon:       fa-check-square-o
navigation: []
internal:   []
external:   []
body:       |

    Here is a general outline of the **key** concepts and commands (arranged by
    topic) that you should know for the **final**.

    The exam will have the following format:

    1. **Definitions**: Briefly define terms using one or two sentences.  (**10
    Points**)

    2. **Short Answers**: Briefly answer questions about:

        - **Hardware** (**5 Points**)

        - **Data Structures / Algorithms** (**5 Points**)

        - **Code Evaluation** (**5 Points**)

        - **Networking** (**5 Points**)

        - **Web Programming** (**10 Points**)

        - **Data & Information** (**5 Points**)

        - **Privacy & Security** (**5 Points**)

    3. **Programming**: Write a [Python] involving [requests] and write a
    [HTML] document. (**10 Points**)

    Parts **1** and **2** is to be done first on paper.  However, part **3**
    can be done with the aid of your laptop and the Internet (but not other
    people).

    <div class="alert alert-warning" markdown="1">
    #### <i class="fa fa-warning"></i> Comprehensive

    The final exam will be a **comprehrensive** assessment. This means that
    everything we have discussed this semester is considered fair game.

    That said, the **majority** of the test will cover the **last half** of the
    class, as detailed below.

    </div>

    [Python]:   https://www.python.org/
    [HTML]:     https://developer.mozilla.org/en-US/docs/Web/HTML
    [CSS]:      https://developer.mozilla.org/en-US/docs/Web/CSS

    ## Terms

    - Internet

    - Protocol

    - URL

    - Client

    - Server

    - IP Address

    - Port

    - DNS

    - HTTP

    - Bandwidth

    - Latency

    - HTML

    - CSS

    - Cloud Computing

    - Cryptography

    - Caesar Cipher

    - Public Key Cryptography

    - Brute-force Attack

    ## Concepts

    ### Networking

    - What are the advantages and disadvantages of different types of
      networking technologies?

    - How do machines communicate on the Internet?

    - How do we measure bandwidth and latency?  What is the difference?

    #### Sample Questions

    > 1. **Rank** the following networking technologies in terms of maximum
    **bandwidth**: dial-up, cable, DSL, fiber optics.

    > 2. Between ethernet and wireless, which one has the better **latency**?
    Explain.

    ### Web Programming

    - What are the components of a [URL] and what do they represent?

    - What happens in a typical HTTP transaction (on both the client and server)?

    - How do we use [HTML] to do the following:

        - Create a document.
        - Set a title.
        - Create a paragraph.
        - Create a header.
        - Specify bold or italic text.
        - Embed an image.
        - Link another document.
        - Enumerate an ordered or unordered list.
        - Construct a table.
        - <p>Create a form with different input types.</p>

    - What is [CSS] and what does it allow us to do?

    - What is the difference between a **static** website and a **dynamic** or
      **interactive** website?

    - How do we use [Tornado] to do the following:

        - Display embedded static web content.
        - Display static web content using templates.
        - Process forms.

    [URL]: https://en.wikipedia.org/wiki/URL
    [Tornado]: https://www.tornadoweb.org/en/stable/web.html

    #### Sample Questions

    > 1. What are the components of the following URL: https://cdt.nd.edu/requirements/
    
    > 2. What happens when you enter a [URL] in your web browser?

    > 3. Given a basic [Tornado] web application, evaluate how the code would
    process a form submission.

    > 4. How do we use [requests] to download web content?

    [requests]: https://2.python-requests.org//en/master/

    ### Data & Information

    - What is cloud computing?

        - What are the motivations for utilizing cloud computing?

        - What are the advantages and disadvantages?

        - <p>What are the ethical concerns regarding cloud computing?</p>

    - How does online advertising impact cloud computing?

        - What is the convenience-surveillance trade-off?

        - What sort of information do companies regularly collect?

        - <p>How do companies use the information that they regularly collect?</p>

    #### Sample Questions

    > 1. What are the pros and cons of using Cloud Computing?

    > 2. What is the convenience-surveillance trade-off and where do you stand?

    ### Privacy & Security

    - What privacy rights do US citizens have?

    - What sort of surveillance does the US government perform?

    - What is public key cryptography used for?

    - What are the ethical, moral, and legal issues regarding cryptography?

    #### Sample Questions

    > 1. Can there be such a thing as "responsible encryption"?  Explain.

    > 2. How would you brute-force attack an iPhone?  What would you do to
    mitigate such an attack?
